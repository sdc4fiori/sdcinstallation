*/---------------------------------------------------------------------\
*| This file is part of SDC4Fiori.                                     |
*|                                                                     |
*| Copyright 2020 SDC4Fiori project members                            |
*|                                                                     |
*| Licensed under the Apache License, Version 2.0 (the "License");     |
*| you may not use this file except in compliance with the License.    |
*| You may obtain a copy of the License at                             |
*|                                                                     |
*|     http://www.apache.org/licenses/LICENSE-2.0                      |
*|                                                                     |
*| Unless required by applicable law or agreed to in writing, software |
*| distributed under the License is distributed on an "AS IS" BASIS,   |
*| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or     |
*| implied.                                                            |
*| See the License for the specific language governing permissions and |
*| limitations under the License.                                      |
*\---------------------------------------------------------------------/

REPORT zsdc_installation.
INCLUDE bdcrecxy .

CONSTANTS c_current_version TYPE string VALUE '1.1.2'.

TYPES:

  BEGIN OF ps_bin_file,
    name TYPE string,
    size TYPE i,
    data TYPE solix_tab,
  END OF ps_bin_file.

DATA : pt_filetab  TYPE filetable,
       lwa_bindata TYPE ps_bin_file.

" Screen
SELECTION-SCREEN BEGIN OF BLOCK part1 WITH FRAME TITLE TEXT-001.
PARAMETERS: p_file LIKE rlgrap-filename OBLIGATORY.
SELECTION-SCREEN END OF BLOCK part1.

SELECTION-SCREEN BEGIN OF BLOCK part2 WITH FRAME TITLE TEXT-002.
PARAMETERS: p_fe TYPE abap_bool AS CHECKBOX,
            p_be TYPE abap_bool AS CHECKBOX.
SELECTION-SCREEN END OF BLOCK part2.

" Process

CLASS ui_process DEFINITION.

  PUBLIC SECTION.
    METHODS constructor.
    METHODS start.
    METHODS upload_file.
    METHODS get_file_name_path IMPORTING iv_filename TYPE string RETURNING VALUE(rv_path) TYPE string.
    METHODS load_transport IMPORTING iv_transport_name TYPE string RETURNING VALUE(rt_message) TYPE wdkmsg_tty.

  PRIVATE SECTION.
    DATA : mv_cofile_path   TYPE trfile,
           mv_data_path     TYPE trfile,
           mt_cofile_buffer TYPE STANDARD TABLE OF epsfili,
           mt_data_buffer   TYPE STANDARD TABLE OF epsfili.
ENDCLASS.

CLASS ui_process IMPLEMENTATION.
  METHOD constructor.
    DATA lt_tefi  TYPE STANDARD TABLE OF stmsttefi.


    CALL FUNCTION 'TMS_TP_CHECK_TRANS_DIR'
      EXPORTING
        iv_target_system     = CONV tmssysnam( sy-sysid )
      TABLES
        tt_tefi              = lt_tefi
      EXCEPTIONS
        permission_denied    = 1
        get_dir_names_failed = 2
        build_path_failed    = 3
        OTHERS               = 4.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    me->mv_data_path = VALUE #( lt_tefi[ dir = 'data' ]-path OPTIONAL ).
    me->mv_cofile_path = VALUE #( lt_tefi[ dir = 'cofiles' ]-path OPTIONAL ).

  ENDMETHOD.

  METHOD start.
    IF p_fe EQ abap_false AND p_be EQ abap_false.
      WRITE : /, icon_alert AS ICON, |A target system should be  specify for the upload.|.
    ELSE.
      upload_file( ).
    ENDIF.

  ENDMETHOD.

  METHOD get_file_name_path.
    FIND  '/' IN me->mv_data_path.
    IF sy-subrc EQ 0.
      DATA(lv_path_pattern) = '/'.
    ELSE.
      lv_path_pattern = '\'.
    ENDIF.
    IF iv_filename CS '_R' ."AND NOT line_exists( me->mt_data_buffer[ name = iv_filename+3 ] ).
      rv_path = |{ me->mv_data_path }{ lv_path_pattern }{ iv_filename+3 }|.
    ELSEIF iv_filename CS '_K' ." AND NOT line_exists( me->mt_cofile_buffer[ name = iv_filename+3 ] ).
      rv_path = |{ me->mv_cofile_path }{ lv_path_pattern }{ iv_filename+3 }|.
    ENDIF.
  ENDMETHOD.

  METHOD load_transport.

    PERFORM bdc_nodata      USING '/'.

    PERFORM open_group      USING '' sy-uname '' sy-datum abap_true.

    PERFORM bdc_dynpro      USING 'SAPMSSY0' '0120'.
    PERFORM bdc_field       USING 'BDC_CURSOR'
                                  '02/03'.
    PERFORM bdc_field       USING 'BDC_OKCODE'
                                  '=APRQ'.
    PERFORM bdc_dynpro      USING 'SAPLTMSU' '0240'.
    PERFORM bdc_field       USING 'BDC_CURSOR'
                                  'WTMSU-REQUEST'.
    PERFORM bdc_field       USING 'BDC_OKCODE'
                                  '=OKAY'.
    PERFORM bdc_field       USING 'WTMSU-REQUEST'
                                  iv_transport_name.
    PERFORM bdc_dynpro      USING 'SAPLSPO1' '0100'.
    PERFORM bdc_field       USING 'BDC_OKCODE'
                                  '=YES'.

    PERFORM bdc_dynpro      USING 'SAPMSSY0' '0120'.
    PERFORM bdc_field       USING 'BDC_OKCODE'
                                  '02/03'.
    PERFORM bdc_field       USING 'BDC_CURSOR'
                                  '=MYF3'.
    PERFORM bdc_transaction TABLES rt_message
    USING                         'STMS_IMPORT'
                                  'X'
                                  'N'
                                  'L'.


    PERFORM close_group USING     'X'.
  ENDMETHOD.

  METHOD upload_file.

    DATA: lv_ret_code TYPE i,
          lv_usr_axn  TYPE i.

    cl_gui_frontend_services=>gui_upload(
        EXPORTING
            filename                = CONV #( p_file )
            filetype                = 'BIN'
          IMPORTING
            filelength              = lwa_bindata-size
          CHANGING
            data_tab                = lwa_bindata-data
          EXCEPTIONS
            file_open_error         = 1
            file_read_error         = 2
            no_batch                = 3
            gui_refuse_filetransfer = 4
            invalid_type            = 5
            no_authority            = 6
            unknown_error           = 7
            bad_data_format         = 8
            header_not_allowed      = 9
            separator_not_allowed   = 10
            header_too_long         = 11
            unknown_dp_error        = 12
            access_denied           = 13
            dp_out_of_memory        = 14
            disk_full               = 15
            dp_timeout              = 16
            not_supported_by_gui    = 17
            error_no_gui            = 18
            OTHERS                  = 19
            ).
    IF sy-subrc <> 0.

      lv_usr_axn = cl_gui_frontend_services=>action_cancel.

      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno

      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      "RAISE ex_dwld_error.
    ENDIF.
    DATA(lx_data) = VALUE xstring( ).

    CALL FUNCTION 'SCMS_BINARY_TO_XSTRING'
      EXPORTING
        input_length = lwa_bindata-size
*       FIRST_LINE   = 0
*       LAST_LINE    = 0
      IMPORTING
        buffer       = lx_data
      TABLES
        binary_tab   = lwa_bindata-data
      EXCEPTIONS
        failed       = 1
        OTHERS       = 2.
    IF sy-subrc <> 0.
* Implement suitable error handling here
    ENDIF.


    DATA(lo_zip) = NEW cl_abap_zip( ).
    lo_zip->load(
      EXPORTING
        zip             = lx_data
      EXCEPTIONS
        zip_parse_error = 1
        OTHERS          = 2
    ).
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
        WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    " On fabrique la condition en fontion de ce que l'on a selectionné FE ou BE
    " Si les deux sont selectionnés alors on cherche les OTs avec un _ (donc tous...)
    DATA(lv_cond) = |name cs '{ COND #(
        WHEN p_be EQ abap_true AND p_fe EQ abap_false THEN 'BE_'
        WHEN p_be EQ abap_false AND p_fe EQ abap_true THEN 'FE_'
        ELSE '_'
    ) }'|.

    " pour chaque fichiers, nous allons les sauvegarder dans le serveur
    LOOP AT lo_zip->files INTO DATA(ls_file) WHERE (lv_cond).
      DATA(lx_content) = VALUE xstring( ).
      lo_zip->get(
        EXPORTING
          name                    = ls_file-name
        IMPORTING
          content                 = lx_content
        EXCEPTIONS
          zip_index_error         = 1
          zip_decompression_error = 2
          OTHERS                  = 3
      ).
      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
          WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

      DATA(lv_file_path) = me->get_file_name_path( ls_file-name ).

      IF lv_file_path IS NOT INITIAL.


        " Sauvegarde le fichier dans le server
        DATA(lv_message) = VALUE string( ).
        OPEN DATASET lv_file_path FOR OUTPUT IN BINARY MODE MESSAGE lv_message .
        TRANSFER lx_content TO lv_file_path.
        CLOSE DATASET lv_file_path .

        IF lv_message IS INITIAL.
          WRITE: / icon_led_green AS ICON,  |File added to { lv_file_path }|.
        ELSE.
          WRITE: / icon_led_red AS ICON,  |Error during writing process for the file { ls_file-name+3 }. The reason is { lv_message }.|.
        ENDIF.

      ELSE.
        WRITE: / icon_led_red AS ICON,  |File { ls_file-name+3 } already exist. Impossible to upload it.|.
      ENDIF.

      IF ls_file-name CS 'FE_K'.
        DATA(lv_tr_fe_name) =  |{ ls_file-name+11(3) }{ ls_file-name+3(7) }|.
      ENDIF.
      IF ls_file-name CS 'BE_K'.
        DATA(lv_tr_be_name) =  |{ ls_file-name+11(3) }{ ls_file-name+3(7) }|.
      ENDIF.
    ENDLOOP.

    " Récupère les deux fichiers si on est en embedded / hu8b

    " Import le fichier et le tester
    IF p_fe = abap_true.
      DATA(lt_message) = me->load_transport( lv_tr_fe_name ).
      LOOP AT lt_message INTO DATA(ls_message) WHERE msgtyp = 'E' OR msgtyp = 'S'.
        IF ls_message-msgtyp = 'E'.
          WRITE: / icon_led_red AS ICON, |Import in STMS_IMPORT does't work for { lv_tr_fe_name }|.
        ELSE.
          WRITE: / icon_led_green AS ICON, |{ lv_tr_fe_name } is in the STMS queue|.
        ENDIF.

      ENDLOOP.
    ENDIF.

    IF p_be = abap_true.
      CLEAR lt_message.
      lt_message = me->load_transport( lv_tr_be_name ).

      LOOP AT lt_message INTO ls_message WHERE msgtyp = 'E' OR msgtyp = 'S'.
        IF ls_message-msgtyp = 'E'.
          WRITE: / icon_led_red AS ICON, |Import in STMS_IMPORT does't work for { lv_tr_be_name }|.
        ELSE.
          WRITE: / icon_led_green AS ICON, |{ lv_tr_be_name } is in the STMS queue|.
        ENDIF.

      ENDLOOP.
    ENDIF.

    " Libérer l'OT

    " Importer l'OT

  ENDMETHOD.

ENDCLASS.

INITIALIZATION.       "to set titlebar on selection screen
  sy-title = |SDC4Fiori Installer Program : v{ c_current_version }|.


AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.
  DATA file_table TYPE filetable.
  DATA rc TYPE i.
  DATA user_action TYPE i.

  cl_gui_frontend_services=>file_open_dialog(
     EXPORTING
       file_filter             = 'ZIP (*.ZIP)|*.zip|All files (*.*)|*.*'
       multiselection          = abap_false
     CHANGING
       file_table              = file_table
       rc                      = rc
       user_action             = user_action ).

  IF rc > 0 AND user_action = cl_gui_frontend_services=>action_ok.
    p_file = file_table[ 1 ].
  ENDIF.



START-OF-SELECTION.
  DATA(lo_ui) = NEW ui_process( ).
  lo_ui->start( ).