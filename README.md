# SDC4Fiori Installation program
## Description
With this application you will be able to upload easily any software component from SDC4Fiori by Saphir package. 

It will unzip and extrat the correct file and upload it on the server in the rigth folder.

Go to the official documentation [here](https://sdc4fiori.gitlab.io/doc/customer/SDCInstallation/)